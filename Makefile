PWD=$(shell pwd)
BUILD_DIR?=$(shell pwd)/build
GOX_OS?=linux
VERSION=$(shell git describe --tags --always)

.PHONY: crosscompile test build run

test:
	go test -v -race -cpuprofile=cpu.out -bench=. -coverprofile=cover.out

coverage: test
	go tool cover -html=cover.out

build: build-dir
	GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o build/lachs cmd/lachs/main.go

build-dir:
	mkdir -p ${BUILD_DIR}/bin

run: build
	docker run -i -t -p 8002:8002 --privileged --rm -v ${PWD}:/data -w /data debian ./build/lachs

debug:
	docker run -i -t -p 8002:8002 --privileged --rm -v ${PWD}:/go/src/gitlab.com/rollbrettler/lachs -w /go/src/gitlab.com/rollbrettler/lachs golang

crosscompile: build-dir
	go get github.com/mitchellh/gox
	gox -ldflags "-X main.version=${VERSION}" -output="${BUILD_DIR}/bin/{{.Dir}}-{{.OS}}-{{.Arch}}" -os="${GOX_OS}" ${GOX_FLAGS} ./...

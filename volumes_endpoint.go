package lachs

import (
	"encoding/json"
	"net/http"
)

func handleVolumes(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		volumes := Volumes{}
		err := volumes.Read()
		if err != nil {
			handle404Error(w, r)
			return
		}
		if len(volumes.List) == 0 {
			handleEmptyVolumesResult(w, r)
			return
		}

		handleVolumesResult(w, r, volumes.List)
	default:
		handle404Error(w, r)
	}
}

func handleEmptyVolumesResult(w http.ResponseWriter, r *http.Request) {
	message := JsonMessage{
		Message: "Empty volumes result",
	}

	w.WriteHeader(http.StatusNoContent)
	writeMessage(w, &message)
}

func handleVolumesResult(w http.ResponseWriter, r *http.Request, volumes []Volume) {
	json, _ := json.Marshal(volumes)
	w.Write(json)
}

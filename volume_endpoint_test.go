package lachs

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os/exec"
	"strings"
	"testing"
)

var volumeUUID = "91c8c155-47cb-4772-9269-7eb78f4a3f72"

func verifyVolumeResponse(t *testing.T, w *httptest.ResponseRecorder) {
	var r Volume

	if w.Code != http.StatusOK {
		t.Errorf("Expected %v got %v", http.StatusOK, w.Code)
	}

	err := json.NewDecoder(w.Body).Decode(&r)
	if err != nil {
		t.Errorf("Parsing the response erorred '%v'", err)
	}

	if r.UUID != volumeUUID {
		t.Errorf("Expected '%v' got '%v'", volumeUUID, r.UUID)
	}

	if r.Unlocked != true {
		t.Errorf("Expected 'true' got '%v'", r.Unlocked)
	}
}

func TestHandleVolumeGet(t *testing.T) {
	req := httptest.NewRequest("GET", "http://example.com/", nil)
	w := httptest.NewRecorder()
	handleVolume(w, req)
	verify404Error(t, w)
}

func TestHandleVolumePost(t *testing.T) {
	req := httptest.NewRequest("POST", "http://example.com/volume/"+volumeUUID,
		strings.NewReader("password=password&mount=/data"))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	w := httptest.NewRecorder()

	execCommand = func(command string, args ...string) *exec.Cmd {
		if command == "/bin/lsblk" {
			return fakeLsblkCommand(command, args...)
		}
		return fakeBlkidCommand(command, args...)
	}
	defer func() { execCommand = exec.Command }()

	execLookPath = func(bin string) (string, error) {
		return "/bin/" + bin, nil
	}
	defer func() { execLookPath = exec.LookPath }()

	handleVolume(w, req)
	verifyVolumeResponse(t, w)
}

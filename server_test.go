package lachs

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func verify404Error(t *testing.T, w *httptest.ResponseRecorder) {
	var r JsonMessage

	if w.Code != http.StatusNotFound {
		t.Errorf("Expected %v got %v", http.StatusNotFound, w.Code)
	}

	err := json.NewDecoder(w.Body).Decode(&r)
	if err != nil {
		t.Errorf("Parsing the response erorred '%v'", err)
	}

	if r.Message != "Not found" {
		t.Errorf("Expected 'Not found' got '%v'", r.Message)
	}
}

func Test404ErrorHandler(t *testing.T) {
	req := httptest.NewRequest("GET", "http://example.com/404", nil)
	w := httptest.NewRecorder()
	handle404Error(w, req)
	verify404Error(t, w)
}

func TestHandleServerError(t *testing.T) {
	req := httptest.NewRequest("GET", "http://example.com/404", nil)
	w := httptest.NewRecorder()
	handleServerError(w, req)
	var r JsonMessage

	if w.Code != http.StatusInternalServerError {
		t.Errorf("Expected %v got %v", http.StatusInternalServerError, w.Code)
	}

	err := json.NewDecoder(w.Body).Decode(&r)
	if err != nil {
		t.Errorf("Parsing the response erorred '%v'", err)
	}

	if r.Message != "Internal server error" {
		t.Errorf("Expected 'Internal server error' got '%v'", r.Message)
	}
}

func TestHandleIndex(t *testing.T) {
	req := httptest.NewRequest("GET", "http://example.com/", nil)
	w := httptest.NewRecorder()
	handleIndex(w, req)
	verify404Error(t, w)
}

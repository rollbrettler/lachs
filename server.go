package lachs

import (
	"encoding/json"
	"log"
	"net/http"
)

type JsonMessage struct {
	Message string `json:"message"`
}

func StartServer() {
	http.HandleFunc("/", logger(handleIndex))
	http.HandleFunc("/volume/", logger(handleVolume))
	http.HandleFunc("/volumes/", logger(handleVolumes))
	http.ListenAndServe(":8002", nil)
}

func writeMessage(w http.ResponseWriter, message *JsonMessage) {
	j, _ := json.Marshal(message)
	w.Write(j)
}

func handle404Error(w http.ResponseWriter, r *http.Request) {
	message := JsonMessage{
		Message: "Not found",
	}

	w.WriteHeader(http.StatusNotFound)
	writeMessage(w, &message)
}

func handleServerError(w http.ResponseWriter, r *http.Request) {
	message := JsonMessage{
		Message: "Internal server error",
	}

	w.WriteHeader(http.StatusInternalServerError)
	writeMessage(w, &message)
}

func logger(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.URL)
		fn(w, r)
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	handle404Error(w, r)
}

package main

import (
	"fmt"
	"log"
	"os"
	"runtime"

	"gitlab.com/rollbrettler/lachs"
)

var version string

func main() {
	log.Printf("Running lachs version %v\n", version)
	switch goos := runtime.GOOS; goos {
	case "linux":
		lachs.StartServer()
	default:
		fmt.Printf("%s is not supported\n", goos)
		os.Exit(0)
	}
}

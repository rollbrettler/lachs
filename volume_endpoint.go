package lachs

import (
	"encoding/json"
	"log"
	"net/http"
)

func handleVolume(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		handleVolumePost(w, r)
	default:
		handle404Error(w, r)
	}
}

func renderVolume(w http.ResponseWriter, r *http.Request, v Volume) {
	json, _ := json.Marshal(v)
	w.Write(json)
}

func handleVolumePost(w http.ResponseWriter, r *http.Request) {
	var password, mount string
	if r.PostFormValue("password") != "" {
		password = r.PostFormValue("password")
	}
	if r.PostFormValue("mount") != "" {
		mount = r.PostFormValue("mount")
	}

	volumes := Volumes{}
	err := volumes.Read()
	if err != nil {
		handle404Error(w, r)
		return
	}

	if len(volumes.List) == 0 {
		handleEmptyVolumesResult(w, r)
		return
	}
	for i := range volumes.List {
		if volumes.List[i].UUID == r.URL.Path[len("/volume/"):] {
			log.Printf("Opening %v at %v\n", volumes.List[i].Path, mount)
			err := volumes.List[i].Open(password, mount)
			if err != nil {
				handleServerError(w, r)
				return
			}
			renderVolume(w, r, volumes.List[i])
		}
	}
}

package lachs

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os/exec"
	"testing"
)

func verifyEmptyVolumesError(t *testing.T, w *httptest.ResponseRecorder) {
	var r JsonMessage

	if w.Code != http.StatusNoContent {
		t.Errorf("Expected %v got %v", http.StatusNoContent, w.Code)
	}

	err := json.NewDecoder(w.Body).Decode(&r)
	if err != nil {
		t.Errorf("Parsing the response erorred '%v'", err)
	}

	if r.Message != "Empty volumes result" {
		t.Errorf("Expected 'Empty volumes result' got '%v'", r.Message)
	}
}

func TestHandleVolumes(t *testing.T) {
	t.Run("Result", func(t *testing.T) {
		req := httptest.NewRequest("GET", "http://example.com/", nil)
		w := httptest.NewRecorder()

		execCommand = func(command string, args ...string) *exec.Cmd {
			if command == "/bin/lsblk" {
				return fakeLsblkCommand(command, args...)
			}
			return fakeBlkidCommand(command, args...)
		}
		defer func() { execCommand = exec.Command }()

		execLookPath = func(bin string) (string, error) {
			return "/bin/" + bin, nil
		}
		defer func() { execLookPath = exec.LookPath }()

		handleVolumes(w, req)

		if w.Code != http.StatusOK {
			t.Errorf("Expected %v got %v", http.StatusOK, w.Code)
		}

		var r []Volume
		err := json.NewDecoder(w.Body).Decode(&r)
		if err != nil {
			t.Errorf("Parsing the response erorred '%v'", err)
		}

		for i := range r {
			if r[i].Path != expectedVolumesStruct[i].Path {
				t.Errorf("Expected %v got %v", expectedVolumesStruct[0].Path, r[i].Path)
			}
			if r[i].UUID != expectedVolumesStruct[i].UUID {
				t.Errorf("Expected %v got %v", expectedVolumesStruct[0].UUID, r[i].UUID)
			}
			if r[i].Type != expectedVolumesStruct[i].Type {
				t.Errorf("Expected %v got %v", expectedVolumesStruct[0].Type, r[i].Type)
			}
		}
	})

	t.Run("Default", func(t *testing.T) {
		req := httptest.NewRequest("POST", "http://example.com/", nil)
		w := httptest.NewRecorder()
		handleVolumes(w, req)
		verify404Error(t, w)
	})

	t.Run("Empty", func(t *testing.T) {
		req := httptest.NewRequest("GET", "http://example.com/", nil)
		w := httptest.NewRecorder()

		execCommand = fakeCommandEmpty
		defer func() { execCommand = exec.Command }()

		execLookPath = func(bin string) (string, error) {
			return "/bin/" + bin, nil
		}
		defer func() { execLookPath = exec.LookPath }()

		handleVolumes(w, req)
		verifyEmptyVolumesError(t, w)
	})

	t.Run("Error", func(t *testing.T) {
		req := httptest.NewRequest("GET", "http://example.com/", nil)
		w := httptest.NewRecorder()

		execCommand = fakeCommandError
		defer func() { execCommand = exec.Command }()

		execLookPath = func(bin string) (string, error) {
			return "/bin/" + bin, nil
		}
		defer func() { execLookPath = exec.LookPath }()

		handleVolumes(w, req)
		verify404Error(t, w)
	})
}

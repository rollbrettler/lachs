package lachs

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"testing"
)

var expectedVolumesStruct = []Volume{
	Volume{
		Path:     "/dev/sda",
		UUID:     "91c8c155-47cb-4772-9269-7eb78f4a3f72",
		Type:     "crypto_LUKS",
		Unlocked: true,
	},
	Volume{
		Path:     "/dev/sdb",
		UUID:     "d47c2b4f-65d2-48cf-bacb-1a2c38e1bc1a",
		Type:     "crypto_LUKS",
		Unlocked: false,
	},
}

var blkidOutput = `
/dev/sda: UUID="91c8c155-47cb-4772-9269-7eb78f4a3f72" TYPE="crypto_LUKS"
/dev/sdb: UUID="d47c2b4f-65d2-48cf-bacb-1a2c38e1bc1a" TYPE="crypto_LUKS"
`
var lsblkOutput = `
disk
crypt
`

func fakeBlkidCommand(command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=TestBlkidHelperProcess", "--", command}
	cs = append(cs, args...)
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	return cmd
}

func fakeLsblkCommand(command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=TestLsblkHelperProcess", "--", command}
	cs = append(cs, args...)
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	return cmd
}

func fakeLuksCommand(command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=TestLuksHelperProcess", "--", command}
	cs = append(cs, args...)
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	return cmd
}

func fakeCommandError(command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=TestErrorHelperProcess", "--", command}
	cs = append(cs, args...)
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	return cmd
}

func fakeCommandEmpty(command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=TestEmptyHelperProcess", "--", command}
	cs = append(cs, args...)
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	return cmd
}

func TestVolume(t *testing.T) {
	execLookPath = func(bin string) (string, error) {
		return "/sbin/" + bin, nil
	}
	defer func() { execLookPath = exec.LookPath }()
	t.Run("Open", func(t *testing.T) {
		execCommand = fakeLuksCommand
		defer func() { execCommand = exec.Command }()

		v := Volume{
			Path: "/path",
			UUID: "UUID",
			Type: "type",
		}
		err := v.Open("password", "mount")
		if err != nil {
			t.Errorf("err: nil != '%v'", err)
		}
	})
	t.Run("OpenError", func(t *testing.T) {
		execCommand = fakeCommandError
		defer func() { execCommand = exec.Command }()

		v := Volume{
			Path: "/path",
			UUID: "UUID",
			Type: "type",
		}
		err := v.Open("password", "mount")
		if err == nil {
			t.Errorf("err: nil == '%v'", err)
		}
	})
}

func TestVolumes(t *testing.T) {
	execCommand = func(command string, args ...string) *exec.Cmd {
		if command == "/bin/lsblk" {
			if args[len(args)-1] == "/dev/sda" {
				return fakeLsblkCommand(command, args...)
			}
			return fakeCommandEmpty(command, args...)
		}
		return fakeBlkidCommand(command, args...)
	}
	defer func() { execCommand = exec.Command }()

	execLookPath = func(bin string) (string, error) {
		return "/bin/" + bin, nil
	}
	defer func() { execLookPath = exec.LookPath }()

	t.Run("Read", func(t *testing.T) {
		var volumes = Volumes{}
		err := volumes.Read()
		if err != nil {
			t.Errorf("err: nil != '%v'", err)
		}

		for i := range volumes.List {
			if volumes.List[i].Type != expectedVolumesStruct[i].Type {
				t.Errorf("type: %v != %v", volumes.List[i].Type, expectedVolumesStruct[i].Type)
			}
			if volumes.List[i].Path != expectedVolumesStruct[i].Path {
				t.Errorf("path: %v != %v", volumes.List[i].Path, expectedVolumesStruct[i].Path)
			}
			if volumes.List[i].UUID != expectedVolumesStruct[i].UUID {
				t.Errorf("uuid: %v != %v", volumes.List[i].UUID, expectedVolumesStruct[i].UUID)
			}
			if volumes.List[i].Unlocked != expectedVolumesStruct[i].Unlocked {
				t.Errorf("unlocked: %v != %v", volumes.List[i].Unlocked, expectedVolumesStruct[i].Unlocked)
			}
		}
	})

	t.Run("execCommandError", func(t *testing.T) {
		execCommand = fakeCommandError
		var volumes = Volumes{}
		err := volumes.Read()
		if err == nil {
			t.Errorf("err: nil == '%v'", err)
		}
	})

	t.Run("execCommandEmpty", func(t *testing.T) {
		execCommand = fakeCommandEmpty
		var volumes = Volumes{}
		err := volumes.Read()
		if err != nil {
			t.Errorf("err: nil != '%v'", err)
		}
		if len(volumes.List) > 0 {
			t.Errorf("%v > 0", len(volumes.List))
		}
	})

	t.Run("execLookPathError", func(t *testing.T) {
		execLookPath = func(bin string) (string, error) {
			return "", errors.New("execLookPathError")
		}
		var volumes = Volumes{}
		err := volumes.Read()
		if err.Error() != "execLookPathError" {
			t.Errorf("err: nil == '%v'", err)
		}
	})
}

func BenchmarkVolumesRead(b *testing.B) {
	execCommand = fakeBlkidCommand
	defer func() { execCommand = exec.Command }()

	execLookPath = func(bin string) (string, error) {
		return "/bin/" + bin, nil
	}
	defer func() { execLookPath = exec.LookPath }()

	var volumes = Volumes{}
	for i := 0; i < b.N; i++ {
		volumes.Read()
	}
}

func TestLuksHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}
	passwordStdin, _ := ioutil.ReadAll(os.Stdin)

	if string(passwordStdin) != "password" {
		t.Errorf("'password' != '%v'", os.Args[3])
	}
	if os.Args[3] != "/sbin/crpytsetup" {
		t.Errorf("'/sbin/crpytsetup' != '%v'", os.Args[3])
	}
	if os.Args[4] != "luksOpen" {
		t.Errorf("'luksOpen' != '%v'", os.Args[4])
	}
	if os.Args[5] != "/path" {
		t.Errorf("'/path' != '%v'", os.Args[5])
	}
	if os.Args[6] != "mount" {
		t.Errorf("'mount' != '%v'", os.Args[6])
	}

	fmt.Fprintf(os.Stdout, "TestLuksHelperProcess output")
	os.Exit(0)
}

func TestBlkidHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}
	fmt.Fprintf(os.Stdout, blkidOutput)
	os.Exit(0)
}

func TestLsblkHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}
	fmt.Fprintf(os.Stdout, lsblkOutput)
	os.Exit(0)
}

func TestEmptyHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}
	fmt.Fprintf(os.Stdout, "")
	os.Exit(0)
}

func TestErrorHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}
	fmt.Fprintf(os.Stdout, "")
	os.Exit(1)
}

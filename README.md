# lachs

api frontend for remote volume decryption

## api

### GET /volumes/

List of volumes that are encrypted

#### curl example

```
curl localhost:8002/volumes/
```

#### Return

```
[
  {
    "path": "/dev/sdb",
    "uuid": "1a5c4aae-e22a-4144-aa0f-aaeb0ef3dde9",
    "type": "crypto_LUKS",
    "unlocked": true
  },
  {
    "path": "/dev/sdc",
    "uuid": "baf1abab-bb8f-42c4-bd88-a0d86d19dfe4",
    "type": "crypto_LUKS",
    "unlocked": false
  }
]
```

### POST /volume/[uuid]

#### Parameter
  - #### password
    *passphrase to decrypt the volume*
  - #### mount
    *will be /dev/mapper/<mount> on linux*

#### curl example

```
curl -X POST -F "password=backup" -F "mount=backup" localhost:8002/volume/baf1abab-bb8f-42c4-bd88-a0d86d19dfe4
```

#### Return

```
{
  "path": "/dev/sdc",
  "uuid": "baf1abab-bb8f-42c4-bd88-a0d86d19dfe4",
  "type": "crypto_LUKS",
  "unlocked": false
}
```
